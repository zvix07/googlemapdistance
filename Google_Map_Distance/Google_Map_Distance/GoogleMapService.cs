﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;

namespace Google_Map_Distance
{
   public class GoogleMapService : IMapService
    {
        public string CreateReqest(string[] originAdress, string[] destinationAdress, string[] mode, string language, string key)
        {
            string newReqest = @"https://maps.googleapis.com/maps/api/distancematrix/json?";

            var origins = "origins=" + string.Join("|", originAdress);
            var destination = "&destinations=" + string.Join("|", destinationAdress);
            var Mode = "&mode=" + string.Join("|", mode);

            return newReqest + origins + destination + Mode + "&language=" + language + "&key=" + key;

        }

        public DistanceElements ParseResponceFromServer(string json)
        {
            return  JsonConvert.DeserializeObject<DistanceElements>(json);
        }

        public string SendReqestToService(string reqest)
        {
            string responseFromServer;
            try
            {
                WebRequest request = WebRequest.Create(reqest);

                WebResponse response = request.GetResponse();

                Stream data = response.GetResponseStream();

                StreamReader reader = new StreamReader(data);

                 responseFromServer = reader.ReadToEnd();

                response.Close();
            }
            catch 
            {
            
                throw;
            }
          

            return responseFromServer;

        }

    }
}