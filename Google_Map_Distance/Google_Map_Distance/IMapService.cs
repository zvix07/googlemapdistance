﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Google_Map_Distance
{
    public interface IMapService
    {

        string CreateReqest(string[] originAdress, string[] destinationAdress, string[] mode, string language, string key);

        string SendReqestToService(string reqest);

        DistanceElements ParseResponceFromServer(string json);



    }
}
