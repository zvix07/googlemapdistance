﻿using System;
using System.Linq;
using Google_Map_Distance;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
using  System.IO;
namespace GoogleMapDistance.Test
{
    [TestClass]
    public class MapServiceTest
    {
        public IMapService mapService = new GoogleMapService();
        private string[] originAdress = new string[] { "Lviv", "Kiev", "London" };
        private string[] destinationAdress = new string[] { "Prague", "Paris", "Krakow", "Berlin" };
        private string[] mode = new string[] { "bicycling", "train", "tram", "subway", "bus" };
        private string language = "en";
        private string key = "AIzaSyDBiR7nm6XurX-IpgnRvx8IzXSGxcdIiUs";


            [TestMethod]
            [ExpectedException(typeof(UriFormatException))]
         public void ResponceIsInvalidSite()
        { 
            mapService.SendReqestToService("vjnnkk");
        }


         [TestMethod]
        public void ResponceIsSuccess()
         {
             string request = mapService.CreateReqest(originAdress, destinationAdress, mode, language, key);
             mapService.SendReqestToService(request);
     
        }

        [TestMethod]
        public  void IsAllDictanceIsSuccess ()
        {
            string request = mapService.CreateReqest(originAdress, destinationAdress, mode, language, key);
            string json =  mapService.SendReqestToService(request);
            DistanceElements result = mapService.ParseResponceFromServer(json);

            bool correct = true;

            foreach (var row in result.rows)
            {
                foreach (var element in row.elements)
                {

                    if (element.status != "OK")
                        correct = false;
                }
            }

            if (result.status != "OK")
                correct = false;

            Assert.IsTrue(correct);
        }

        [TestMethod]
        public void IsParserWorkCorrect()
         {
             string request = mapService.CreateReqest(originAdress, destinationAdress, mode, language, key);
             string json = mapService.SendReqestToService(request);
             DistanceElements result = mapService.ParseResponceFromServer(json);

             var correct = true;

             foreach (var row in result.rows)
             {
                 if (row.elements.Length != destinationAdress.Length)
                     correct = false;
             }

             if (result.rows.Length != originAdress.Length)
                 correct = false;

             Assert.IsTrue(correct);
         }
    }
}
